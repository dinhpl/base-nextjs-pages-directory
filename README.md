# NextJS Boilerplate

## Tech stack

- NextJS (version current 13.14 - Page router)
- ReactJS (version current 18.2.0)
- Redux toolkit (version latest)
- React-hook-form (version latest)
- Tailwindcss (version latest)
- Antd (version current 5.8.2)

## Getting Started

To get a local copy up and running, please follow these simple steps.

### Prerequisites

Here is what you need to be able to run this project.

- Node.js (version: >= 16.14)
- Yarn or NPM (but Yarn recommended)

## Development

### Setup

- Clone the repo into a public GitHub repository

```
git clone https://gitlab.com/dinhpl/base-nextjs-pages-directory
```

- Go to the project folder

```
cd base-nextjs-project-directory
```

- Install packages with yarn

```
yarn install
```

- Set up your `.env` file
  - Duplicate `.env.dev` to `.env`
  - Use `openssl rand -base64 32` to generate a key and add it under `NEXTAUTH_SECRET` in the `.env` file.

* Start development

```
yarn dev
```

- For a production build

```
yarn build
yarn start
```

## Enjoy at the moment
