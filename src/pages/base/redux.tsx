import Layout from '@/layouts/main'
import { useDispatch, useSelector } from '@/redux/store'
import { updateUsername } from '@/redux/slices/users'

export default function Redux() {
  const dispatch = useDispatch()
  const { username } = useSelector(state => state.user)

  const handleClickButton = () => {
    dispatch(updateUsername('test_user'))
  }

  return (
    <>
      <div>username: {username}</div>
      <div className='flex items-center justify-center'>
        <button onClick={handleClickButton}>Update Name</button>
      </div>
    </>
  )
}
Redux.getLayout = function getLayout(page: React.ReactElement) {
  return <Layout>{page}</Layout>
}
