import React from 'react'
import Image from 'next/image'
import image from '@assets/images/image_1.png'
import Layout from '@/layouts/main'
import LoginForm from '@/containers/auth/LoginForm'

export default function BaseLoginPage() {
  return (
    <div className={'flex h-full w-full flex-col items-center justify-center'}>
      <div className='flex h-full w-full items-center justify-around'>
        <div>
          <Image src={image} width={500} height={500} alt='Picture of the author' />
        </div>
        <div className='flex h-[50%] w-[30%] flex-col justify-center rounded-lg bg-white p-12 shadow'>
          <div>
            <h1 className='mb-4 text-[2.5rem] font-bold'>Login</h1>
          </div>
          <LoginForm />
        </div>
      </div>
    </div>
  )
}
BaseLoginPage.getLayout = function getLayout(page: React.ReactElement) {
  return <Layout>{page}</Layout>
}
