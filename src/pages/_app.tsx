import '@/styles/globals.scss'
import type { AppProps } from 'next/app'
import type { ReactElement, ReactNode } from 'react'
import type { NextPage } from 'next'
import { Provider as ReduxProvider } from 'react-redux'
import { store } from '@/redux/store'
import NextAuthProviders from '@/libs/NextAuthProviders'
import { ConfigProvider } from 'antd'

type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode
}

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout
}
export default function App({ Component, pageProps }: AppPropsWithLayout) {
  const getLayout = Component.getLayout ?? (page => page)
  return (
    <NextAuthProviders>
      <ConfigProvider>
        <ReduxProvider store={store}>{getLayout(<Component {...pageProps} />)}</ReduxProvider>
      </ConfigProvider>
    </NextAuthProviders>
  )
}
