import ja from '../../public/lang/ja.js'

const useTranslate = () => (key, fallback) => {
  let trans = ja
  const keys = key.split('.')
  for (const k of keys) {
    if (trans[k] === undefined) {
      return fallback
    }
    trans = trans[k]
  }
  return trans
}

export default useTranslate
