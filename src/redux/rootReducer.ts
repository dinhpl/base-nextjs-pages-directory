import { combineReducers } from 'redux'

// slices
import userReducer from './slices/users'

const rootReducer = combineReducers({
  user: userReducer,
})

export default rootReducer
