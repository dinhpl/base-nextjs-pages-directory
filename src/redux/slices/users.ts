import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  isLoading: false,
  error: null,
  username: 'Guest',
}

const slice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    updateUsername: (state, actions) => {
      state.username = actions.payload
    },
  },
})

// Reducer
export default slice.reducer

// Actions
export const { updateUsername } = slice.actions
