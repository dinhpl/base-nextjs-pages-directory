import React from 'react'
import Link from 'next/link'
import { Button } from 'antd'
import { signOut, useSession } from 'next-auth/react'

const HeaderComponent = () => {
  const { data: session, status } = useSession()
  const isLogin = !!session

  return (
    <header>
      <nav className='flex h-[7rem] items-center border-[#12202F] bg-[#12202F] px-[7rem] text-white'>
        <div className='flex w-full flex-wrap items-center justify-between'>
          {/* LEFT */}
          <div>
            <Link href='/' className='flex items-center'>
              <span className='self-center whitespace-nowrap text-[2rem] font-semibold'>NEXTJS BASE</span>
            </Link>
          </div>
          {/* CENTER */}
          <div className='ml-[5rem] flex-1 items-center justify-between'>
            <ul className='flex space-x-8'>
              <li>
                <a
                  href='#'
                  className='bg-primary-700 block rounded p-0 text-white hover:text-[#EF4230]'
                  aria-current='page'
                >
                  Menu
                </a>
              </li>
              <li>
                <a href='#' className='bg-primary-700 block rounded p-0 text-white hover:text-[#EF4230]'>
                  Company
                </a>
              </li>
              <li>
                <a href='#' className='bg-primary-700 block rounded p-0 text-white hover:text-[#EF4230]'>
                  Features
                </a>
              </li>
              <li>
                <Link href='/base/about' className='bg-primary-700 block rounded p-0 text-white hover:text-[#EF4230]'>
                  About
                </Link>
              </li>
              <li>
                <Link href='/base/redux' className='bg-primary-700 block rounded p-0 text-white hover:text-[#EF4230]'>
                  Redux
                </Link>
              </li>
              <li>
                <Link href='/base/loading' className='bg-primary-700 block rounded p-0 text-white hover:text-[#EF4230]'>
                  Loading
                </Link>
              </li>
            </ul>
          </div>
          {/* RIGHT */}
          <div>
            {!isLogin && status !== 'loading' ? (
              <div className='flex items-center'>
                <Link href='#' className='mr-[1rem]'>
                  <Button type='primary' shape='round'>
                    SignUp
                  </Button>
                </Link>
                <Link href='/base/auth/signin'>
                  <Button type='primary' shape='round'>
                    SignIn
                  </Button>
                </Link>
              </div>
            ) : (
              <div className='flex items-center'>
                <div className='mr-4'>{session?.user?.email}</div>
                <Button type='primary' shape='round' onClick={() => signOut()}>
                  Sign Out
                </Button>
              </div>
            )}
          </div>
        </div>
      </nav>
    </header>
  )
}
export default HeaderComponent
