import React from 'react'
import { Input, Typography, InputProps } from 'antd'
import { useFormContext, Controller } from 'react-hook-form'

type FormTextFieldProps = InputProps & {
  name: string
}

const { Text } = Typography

export default function FormTextField({ name, ...others }: FormTextFieldProps) {
  const { control } = useFormContext()

  return (
    <Controller
      name={name}
      control={control}
      render={({ field, fieldState: { error } }) => (
        <>
          <Input
            {...field}
            value={typeof field.value === 'number' && field.value === 0 ? '' : field.value}
            status={!!error ? 'error' : ''}
            {...others}
          />
          {error ? (
            <Text strong type='danger'>
              {error?.message}
            </Text>
          ) : null}
        </>
      )}
    />
  )
}
