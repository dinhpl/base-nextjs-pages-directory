import React from 'react'
import * as Yup from 'yup'
import { Button, Space } from 'antd'
import { signIn } from 'next-auth/react'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { useRouter, useSearchParams } from 'next/navigation'
import FormProvider from '@/components/HookForm/FormProvider'
import { FormTextField, FormTextFieldPassword } from '@/components/HookForm/FormControl'

type FormValuesProps = {
  email: string
  password: string
}

export default function LoginForm() {
  const router = useRouter()
  const searchParams = useSearchParams()

  const LoginSchema = Yup.object().shape({
    email: Yup.string().required('Email is required').email('Email must be a valid email address'),
    password: Yup.string().required('Password is required'),
  })

  const defaultValues = {
    email: '',
    password: '',
  }

  const methods = useForm<FormValuesProps>({
    resolver: yupResolver(LoginSchema),
    defaultValues,
  })

  const {
    reset,
    setError,
    handleSubmit,
    formState: { errors, isSubmitting, isSubmitSuccessful },
  } = methods

  const onSubmit = async (data: FormValuesProps) => {
    try {
      const response = await signIn('credentials', {
        ...data,
        redirect: false,
        callbackUrl: searchParams?.get('from') || '/',
      })

      if (response?.ok) {
        router.push(searchParams?.get('from') || '/')
      }
    } catch (error) {
      console.log(error)
      reset()
    }
  }

  return (
    <FormProvider methods={methods} onSubmit={handleSubmit(onSubmit)}>
      <Space.Compact direction='vertical' block className='mb-4'>
        <FormTextField name='email' placeholder='Enter your email' />
      </Space.Compact>
      <Space.Compact direction='vertical' block className='mb-4'>
        <FormTextFieldPassword name='password' placeholder='Enter your password' />
      </Space.Compact>
      <Button type='primary' htmlType='submit' loading={isSubmitting}>
        Submit
      </Button>
    </FormProvider>
  )
}
